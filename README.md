# The project

This is just a sample project to show how to animate a page intro with the JavaScript library [GSAP][gsap] by GreenSock.
* The **app-final.js** linked with index-final.html is a proposition of animation
* The **app.js** linked with index.html is the work file

![Tesla Cybertruck WebDesign](assets/img/thumbnail.jpg "Tesla Cybertruck WebDesign")

# License
- License: [WTFPL][wtfpl]
- Concept product, design and images: [Tesla][tesla]
- Web Design: [Raphaël Aymoz][raymoz]
- Web Integration & animation: [Ancelio Massing][amassing]
- Contributors: [Janou Crastes][jcrastes]


# Requirements

GSAP3: all dependancies are in the project main folder


# Installation

1. Go to the [project link][project-link]
2. Click "Clone" if you work with terminal, if not click "download"
3. Work and Enjoy !



[gsap]: https://greensock.com/gsap/
[wtfpl]: http://www.wtfpl.net/about/
[tesla]: https://www.tesla.com/cybertruck
[raymoz]: https://raphaelaymoz.fr
[amassing]: https://amassing.fr
[jcrastes]: https://linkedin.com/in/janou-crastes
[project-link]: https://gitlab.com/Amassing/tesla-cybertruck-animation-page-gsap
