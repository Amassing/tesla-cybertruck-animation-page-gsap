window.onload = function(){

    // ANIMATIONS WITH GSAP
    let timeline = gsap.timeline()

    timeline
        .addLabel('start')
        .to('.loader', {
            duration: 2.5,
            opacity: 0,
            onComplete: function () {
                document.querySelector('.loader').hidden = true
            }
        })        
        .from('.img-back', { // Background image
            duration: 1.8,
            x: "50%"
        }, 'start')
        .from('.navbar-nav li', { // Menu item
            duration: 1.5,
            x: -200,
            opacity: 0,
            stagger: -0.1
        }, 'start')
        .from('.slider-bar-active', { // Slider bar
            duration: 1.8,
            y: 400,
            opacity: 0
        }, 'start')
        .from('.slider-counter', { // Background counter
            duration: 1.8,
            delay: 0.3,
            opacity: 0
        }, 'start')
        .from('.content-text, .content-text a', { // Btns group
            duration: 0.8,
            opacity: 0,
            y: 8,
            ease: "power1.out",
            stagger: 0.1
        }, '-=0.8')

        // SVG animation
        .addLabel('svg-animation')
        .to('.svg-tag-animate', { // SVG container visibility
            duration: 1,
            opacity: 1,
        }, 'svg-animation')
        .to('.svg-tag-animate path', { // SVG Move strokes
            duration: 4,
            strokeDashoffset: 0,
            strokeDasharray: 0
        }, 'svg-animation')
        .to('.svg-tag-animate path', { // SVG Fill colors
            duration: 0.8,
            delay: 3,
            fill: 'white'
        }, 'svg-animation')
        .fromTo('.svg-tag-animate', {            
            scale: 1,
        }, {
            duration: 0.3,
            repeat: 1,
            yoyo: true,
            scale: 1.02,
            ease: 'expo.in'
        }, '-=0.5')

        // Filter saturation
        .fromTo('.img-back', { // Background image
            filter: 'saturate(0)'
        },
        {
            duration: 1,
            filter: 'saturate(1)'
        }, '-=0.7')


        // DISPLAY FOOTER LICENSE
        document.querySelectorAll('.btn, .navbar-nav a').forEach(item => {
            item.addEventListener('click', () => {
                document.querySelector('.footer').classList.toggle('is-active')
            })
        })

}