window.onload = function(){

    // DISPLAY FOOTER LICENSE
    document.querySelectorAll('.btn, .navbar-nav a').forEach(item => {
        item.addEventListener('click', () => {
            document.querySelector('.footer').classList.toggle('is-active')
        })
    })

    

    // ANIMATIONS WITH GSAP
    let timeline = gsap.timeline()

    timeline
        .addLabel('start')       
        .from('.img-back', {
            duration: 1.8,
            x: "50%"
        }, 'start')
    
}